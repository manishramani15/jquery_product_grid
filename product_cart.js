function ProductCart() {
  this.json = [];
  this.arrayFilter = {};
  this.filterOptions = ['brand', 'color', 'sold_out'];
  this.imagesContainer = $('<div>', {'data-behaviour': 'imagesContainer', 'class': 'imagesContainer'});
}

ProductCart.prototype.init = function() {
  this.createArrayFilter();
  this.makeAjaxRequest();
};

ProductCart.prototype.createArrayFilter = function() {
  var _this = this;
  this.filterOptions.forEach(function(element) {
    _this.arrayFilter[element] = [];
  });
};

ProductCart.prototype.makeAjaxRequest = function() {
  var _this = this;
  $.ajax({
      url: 'product.json',
      dataType: 'json',
    })
  .done(function(json) {
    _this.json = json;
    _this.createFilters();
    _this.displayImages();
    _this.bindEvents();
  });
};

ProductCart.prototype.createFilters = function() {
  var _this = this,
      filterContainer = $('<div>');
  this.filterOptions.forEach(function(element) {
    var elementContainer = [],
        container = $('<div>', {'data-type' : 'container','data-behaviour': element}),
        heading = $('<h3>', {'text': element}),
        list = $('<ul>');
    container.append(heading);
    _this.createFilterContainers(elementContainer, element);
    _this.createFilterContainerContents(elementContainer, list);
    container.append(list).appendTo(filterContainer);
  });
  $('[data-behaviour="filter"]').append(filterContainer);
};

ProductCart.prototype.createFilterContainerContents = function(elementContainer, list) {
  elementContainer = Array.from(new Set(elementContainer));
  elementContainer.forEach(function(element) {
    var checkBox = $('<input>', {'type': 'checkbox','data-text': element}),
        listItem = $('<li>', {'text': element});
    listItem.append(checkBox).appendTo(list);
  });
};

ProductCart.prototype.createFilterContainers = function(elementContainer, element) {
  var _this = this;
  this.json.forEach(function(jsonElement) {
    elementContainer.push(jsonElement[element]);
  });
};

ProductCart.prototype.bindEvents = function() {
  var _this = this;
  $('input[type="checkbox"]').on('click', function() {
    _this.imagesContainer.empty();
    var $this = $(this),
        value = $this.data('text'),
        prop = $this.closest('[data-type="container"]').data('behaviour');
    if (this.checked) {
      _this.arrayFilter[prop].push(value);
    } else {
      _this.arrayFilter[prop] = _this.arrayFilter[prop].filter(function(element) {
        return element !== value;
      });
    }
    _this.collectFilteredImages();
  });
};

ProductCart.prototype.collectFilteredImages = function() {
  var _this = this,
      result = this.json.filter(function(element) {
        return _this.filterOptions.every(function(filterOption) {
          return (_this.arrayFilter[filterOption].toString().includes(element[filterOption]) ||
            _this.arrayFilter[filterOption].length === 0)
        })
      });
  this.displayImages(result);
};

ProductCart.prototype.displayImages = function(result = this.json) {
  var _this = this;
  result.forEach(function(element) {
    $('<img>').prop('src', `images/${element.url}`).appendTo(_this.imagesContainer);
  });
  this.imagesContainer.appendTo('[data-behaviour="display"]');
};

$(function() {
  var productCart = new ProductCart();
  productCart.init();
})